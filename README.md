# OmegaMUD Bin
Release binaries and media files for the OmegaMUD project.

## OmegaMUD Project Links
- http://www.OmegaMUD.com
- https://gitlab.com/beckersource/OmegaMUD

## Media Folders
- ![Screenshots](./screenshots)
- ![Videos](./videos)

## Latest Video
![Latest Video](./videos/omegamud_2024-04-14.mp4)

## Latest Screenshot: 2024-04-05: GUI, Combat, & Various
![Latest Screenshot](./screenshots/omud_2024-04-05_gui+combat+various.png)
